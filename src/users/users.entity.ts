import { BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import * as crypto from "crypto";

@Entity('users')
export class UsersEntity {

  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @BeforeInsert()
  @BeforeUpdate()
  hashPassword() {
    const salt = crypto.randomBytes(16).toString("hex");
    this.password = crypto.pbkdf2Sync(this.password, salt, 10, 16, 'sha256').toString('hex');
  }

}
