import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsersDTO } from './users.dto';
import { UsersEntity } from './users.entity';

@Injectable()
export class UsersService {

  constructor(
    @InjectRepository(UsersEntity) 
    private userRepository: Repository<UsersEntity>) {
    }

    async findAll() {
      return await this.userRepository.find();
    }

    async createUser(data: UsersDTO) {
      const user = this.userRepository.create(data);
      return await this.userRepository.save(user);
    }

    async findByEmail(email: string): Promise<UsersDTO> {
      return await this.userRepository.findOne({
        where: {
          email: email
        }
      });
    }

    async findById(id: number) {
      return await this.userRepository.findOne({where : {id: id}});
    }

    async updateById(id: number, data: Partial<UsersDTO>) {
      const updatedValues = this.userRepository.create(data);
      return await this.userRepository.update(id, updatedValues);
    }

    async delete(id: number) {
      await this.userRepository.delete({ id });
      return { deleted: true };
    }

}
