import { Controller, Delete, Get, Post, HttpStatus, Body, Param, Put } from '@nestjs/common';
import { UsersDTO } from './users.dto';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {

  constructor(private usersService: UsersService) {}

  @Get()
  async findAll() {
    const users = await this.usersService.findAll();
    return {
      statusCode: HttpStatus.OK,
      mesage: `users found`,
      users
    }
  }

  @Post()
  async createUsers(@Body() data: UsersDTO) {
    const user = await this.usersService.createUser(data);
    return {
      statusCode: HttpStatus.OK,
      message: 'User created successfully',
      user
    };
  }

  @Get(':id')
  async readUser(@Param('id') id: number) {
    const user =  await this.usersService.findById(id);
    return {
      statusCode: HttpStatus.OK,
      message: 'User fetched successfully',
      user,
    };
  }

  @Put(':id')
  async uppdateUser(@Param('id') id: number, @Body() data: Partial<UsersDTO>) {
    const updatedUser = await this.usersService.updateById(id, data);
    return {
      statusCode: HttpStatus.OK,
      message: 'User updated successfully',
      updatedUser: updatedUser,
    };
  }

  @Delete(':id')
  async deleteUser(@Param('id') id: number) {
    await this.usersService.delete(id);
    return {
      statusCode: HttpStatus.OK,
      message: 'User deleted successfully',
    };
  }

}
